This project comprises a GUI application for viewing real-time sensor information (e.g. temperature, humidity etc) in a ROS network.
Also, a logger for recording a subset of ROS topics is included. The resulting bag file can be played back through the terminal,
by typing the command 'rosbag play ...' 

In order to run the project, please make sure that there is a running (Hydro) ROS network and that the dependencies listed in CMakeLists.txt
are valid for your system, with the paths adapted accordingly (i.e., for roscpp, qt, qwt, boost among others). The versions used were qt 4.8.4 
and qwt 5.2.4.