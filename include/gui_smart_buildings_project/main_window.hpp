/**
 * @file /include/gui_smart_buildings_project/main_window.hpp
 *
 * @brief Qt based gui for gui_smart_buildings_project.
 *
 * @date November 2010
 **/
#ifndef gui_smart_buildings_project_MAIN_WINDOW_H
#define gui_smart_buildings_project_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"
#include "../include/all_msg_listener_node.h"
#include "boost/thread.hpp"
#include "QVector"
#include "qwt_plot_curve.h"
/*****************************************************************************
** Namespace
*****************************************************************************/
#define MAX_VALUES 100

namespace gui_smart_buildings_project {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT
SubscriberManager *s;
QVector<double> tempr1;
QVector<double> tempr2;
QVector<double> humid1;
QVector<double> humid2;
QVector<double> lux1;
QVector<double> lux2;
QVector<double> ir1;
QVector<double> ir2;
QVector<double> broadb1;
QVector<double> broadb2;
QVector<double> door;
QwtPlotCurve *tempr1_curve;
QwtPlotCurve *tempr2_curve;
QwtPlotCurve *humid1_curve;
QwtPlotCurve *humid2_curve;
QwtPlotCurve *lux1_curve;
QwtPlotCurve *lux2_curve;
QwtPlotCurve *ir1_curve;
QwtPlotCurve *ir2_curve;
QwtPlotCurve *broadb1_curve;
QwtPlotCurve *broadb2_curve;
QTimer *timer;
std::string previous;
int peopleCounter;
public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();

public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
	void on_actionAbout_triggered();
        void updateSensorInfo();
private:
	Ui::MainWindowDesign ui;
	QNode qnode;
};

}  // namespace gui_smart_buildings_project

#endif // gui_smart_buildings_project_MAIN_WINDOW_H
