/*
 * Logger for messages published by a set of ROS nodes. A running ROS master is required
 * on the same network. The resulting bag can be played back through the 'rosbag play' command.
 * @date 23-07-2014
 * @author Christos Karaiskos
*/

#ifndef __BAG_WRITER_H
#define __BAG_WRITER_H 

#include <string>
#include <vector>
#include "ros/ros.h"
#include "rosbag/bag.h"
#include "rosbag/recorder.h"


namespace sml
{
  class SensorMsgLogger {
  private:
    rosbag::RecorderOptions opts;                               //logging options
  public:
    SensorMsgLogger(int maxDuration, int maxSizeMB, bool recordAll, std::string bagfilename);
    void listAllTopics();                                       //print all available topics
    std::vector<std::string> getAllTopics();                    //get all available topics as a vector
    void logAllTopics();                                        //log all published topics that are discoverable by ros core
    bool isTopicAvailable(const std::string & s);               //check whether a topic with a given name is available
    void logTopicsWithNames(std::vector<std::string> & topics); //log specific topics, the names of which are given in the 'topics' vector

    void setMaxLoggingDurationInSeconds(int duration);          //set logger to stop after recording a maximum of 'duration' secs
    void setMaxBagSizeInMB(int sizeMB);                         //set logger to stop after recording a maximum of sizeMB megabytes
    void setRecordAllTopics(bool recordAll);                    //set logger to record all msgs on all topics
    void setBagFileName(std::string filename);                  //set bag file name

    ros::Duration getMaxLoggingDurationInSeconds();
    int getMaxBagSize();
    bool doesRecordAllTopics();
    std::string getBagFileName();
  };
}

#endif 
