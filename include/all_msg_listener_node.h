/*
 * Listener for messages published by a list of ROS sensor nodes. 
 * The implemented project included two temperature sensors, 
 * two humidity sensors, two light sensors and a door passage detector.
 * @date 23-07-2014
 * @author Christos Karaiskos
*/
#ifndef __SUBSCRIBER_MANAGER_H
#define __SUBSCRIBER_MANAGER_H

#include "ros/ros.h"
#include "smartbuildings/Light.h"
#include "smartbuildings/Temp.h"
#include "smartbuildings/Humid.h"
#include "std_msgs/String.h"

class SubscriberManager {
private:
  float temperature1;
  float temperature2;
  float humidity1;
  float humidity2;
  int lux1;
  int broadband1;
  int ir1;
  int lux2;
  int broadband2;
  int ir2;
  std_msgs::String door;

  //callbacks
  void light1_broadband_callback(const smartbuildings::Light &lightBroadband);
  void light1_ir_callback(const smartbuildings::Light &lightIR);
  void light1_lux_callback(const smartbuildings::Light &lightLux);
  void light2_broadband_callback(const smartbuildings::Light &lightBroadband);
  void light2_ir_callback(const smartbuildings::Light &lightIR);
  void light2_lux_callback(const smartbuildings::Light &lightLux);
  void temperature1_callback(const smartbuildings::Temp &temp);
  void humidity1_callback(const smartbuildings::Humid &humid);
  void temperature2_callback(const smartbuildings::Temp &tempr);
  void humidity2_callback(const smartbuildings::Humid &humid);
  void door_callback(const std_msgs::String &s);
public:
  SubscriberManager();

  //getters
  float getTemperatureFromSensor1();
  float getTemperatureFromSensor2();
  float getHumidityFromSensor1();
  float getHumidityFromSensor2();
  int getLuxFromSensor1();
  int getLuxFromSensor2();
  int getIRFromSensor1();
  int getIRFromSensor2();
  int getBroadbandFromSensor1();
  int getBroadbandFromSensor2();
  std_msgs::String getDoorSensor();
 
  //setters
  void setTemperatureFromSensor1(float tempr);
  void setTemperatureFromSensor2(float tempr);
  void setHumidityFromSensor1(float humid);
  void setHumidityFromSensor2(float humid);
  void setLuxFromSensor1(int lux);
  void setLuxFromSensor2(int lux);
  void setIRFromSensor1(int ir);
  void setIRFromSensor2(int ir);
  void setBroadbandFromSensor1(int broadband);
  void setBroadbandFromSensor2(int broadband);
  void setDoorSensor(std_msgs::String d);

  //starts saving state information about all subscribed ROS nodes
  void subscribeToAllTopics(int argc, char** argv);
};

#endif
