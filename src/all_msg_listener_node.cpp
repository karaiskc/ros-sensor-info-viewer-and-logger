/*
 * Listener for messages published by a list of ROS sensor nodes. 
 * The implemented project included two temperature sensors, 
 * two humidity sensors, two light sensors and a door passage detector.
 * @date 23-07-2014
 * @author Christos Karaiskos
*/
#include "all_msg_listener_node.h"
#include "ros/spinner.h"


using namespace std;

SubscriberManager::SubscriberManager() {
  temperature1 = 0.0;
  temperature2 = 0.0;
  humidity1 = 0.0;
  humidity2 = 0.0;
  lux1 = 0;
  lux2 = 0;
  broadband1 = 0;
  broadband2 = 0;
  ir1 = 0;
  ir2 = 0;
}

//getters

float SubscriberManager::getTemperatureFromSensor1() {
   return temperature1;
}

float SubscriberManager::getTemperatureFromSensor2() {
  return temperature2;
}

float SubscriberManager::getHumidityFromSensor1() {
  return humidity1;
}

float SubscriberManager::getHumidityFromSensor2() {
  return humidity2;
}

int SubscriberManager::getLuxFromSensor1() {
  return lux1;
}

int SubscriberManager::getLuxFromSensor2() {
  return lux2;
}

int SubscriberManager::getIRFromSensor1() {
  return ir1;
}

int SubscriberManager::getIRFromSensor2() {
  return ir2;
}

int SubscriberManager::getBroadbandFromSensor1() {
  return broadband1;
}

int SubscriberManager::getBroadbandFromSensor2() {
  return broadband2;
}

std_msgs::String SubscriberManager::getDoorSensor() {
    return door;
}

//setters

void SubscriberManager::setTemperatureFromSensor1(float tempr) {
  temperature1 = tempr;
}

void SubscriberManager::setTemperatureFromSensor2(float tempr) {
  temperature2 = tempr;
}

void SubscriberManager::setHumidityFromSensor1(float humid) {
  humidity1 = humid;
}

void SubscriberManager::setHumidityFromSensor2(float humid) {
  humidity2 = humid;
}

void SubscriberManager::setLuxFromSensor1(int lux) {
  lux1 = lux;
}

void SubscriberManager::setLuxFromSensor2(int lux) {
  lux2 = lux;
}

void SubscriberManager::setIRFromSensor1(int ir) {
  ir1 = ir;
}

void SubscriberManager::setIRFromSensor2(int ir) {
  ir2 = ir;
}

void SubscriberManager::setBroadbandFromSensor1(int broadband) {
  broadband1 = broadband;
}

void SubscriberManager::setBroadbandFromSensor2(int broadband) {
  broadband2 = broadband;
}

void SubscriberManager::setDoorSensor(std_msgs::String s) {
  door = s;
}


//private member functions

void SubscriberManager::light1_broadband_callback(const smartbuildings::Light &lightBroadband) {
  //  ROS_INFO("[Light Sensor #1] broadband = %d", lightBroadband.light);
  broadband1 = lightBroadband.light;
}

void SubscriberManager::light1_ir_callback(const smartbuildings::Light & lightIR) {
  //  ROS_INFO("[Light Sensor #1] ir = %d", lightIR.light);
  ir1 = lightIR.light;
}

void SubscriberManager::light1_lux_callback(const smartbuildings::Light &lightLux) {
  //  ROS_INFO("[Light Sensor #1] lux =  %d", lightLux.light);
  lux1 = lightLux.light;
}

void SubscriberManager::light2_broadband_callback(const smartbuildings::Light &lightBroadband) {
  //  ROS_INFO("[Light Sensor #2] broadband =  %d", lightBroadband.light);
  broadband2 = lightBroadband.light;
}

void SubscriberManager::light2_ir_callback(const smartbuildings::Light & lightIR) {
  //  ROS_INFO("[Light Sensor #2] ir = %d", lightIR.light);
  ir2 = lightIR.light;
}

void SubscriberManager::light2_lux_callback(const smartbuildings::Light &lightLux) {
  //  ROS_INFO("[Light Sensor #2] lux = %d", lightLux.light);
  lux2 = lightLux.light;
}

void SubscriberManager::temperature1_callback(const smartbuildings::Temp &temp) {
  //  ROS_INFO("[Tempr Sensor #1] temperature = %f", temp.temp);
  temperature1 = temp.temp;
}

void SubscriberManager::humidity1_callback(const smartbuildings::Humid &humid) {
  //  ROS_INFO("[Humid Sensor #1] humidity = %f", humid.humid);
  humidity1 = humid.humid;
}

void SubscriberManager::temperature2_callback(const smartbuildings::Temp &temp) {
  //  ROS_INFO("[Tempr Sensor #2] temperature = %f", temp.temp);
  temperature2 = temp.temp;
}

void SubscriberManager::humidity2_callback(const smartbuildings::Humid &humid) {
  //  ROS_INFO("[Humid Sensor #2] humidity = %f", humid.humid);
  humidity2 = humid.humid;
}

void SubscriberManager::door_callback(const std_msgs::String &d) {
  //  ROS_INFO("[Humid Sensor #2] humidity = %f", humid.humid);
  door = d;
}

void SubscriberManager::subscribeToAllTopics(int argc, char** argv) {
  ros::init(argc,argv, "all_sensor_listener");
  ros::NodeHandle n;
  
//topic names have been predefined for each sensor, feel free to change
  ros::Subscriber sensor1_broadband_sub = n.subscribe("/smartbuildings/light/broadband", 1000, &SubscriberManager::light1_broadband_callback,this);
  ros::Subscriber sensor1_ir_sub = n.subscribe("/smartbuildings/light/ir", 1000, &SubscriberManager::light1_ir_callback,this);
  ros::Subscriber sensor1_lux_sub = n.subscribe("/smartbuildings/light/lux",1000,&SubscriberManager::light1_lux_callback,this);

  //subscribers to light sensor 2 topics
  ros::Subscriber sensor2_broadband_sub = n.subscribe("/smartbuildings/light2/broadband",1000,&SubscriberManager::light2_broadband_callback,this);
  ros::Subscriber sensor2_ir_sub = n.subscribe("/smartbuildings/light2/ir",1000,&SubscriberManager::light2_ir_callback,this);
  ros::Subscriber sensor2_lux_sub = n.subscribe("/smartbuildings/light2/lux",1000,&SubscriberManager::light2_lux_callback,this);
  
  //subscribers to temperature sensor 1 topics
  ros::Subscriber temperature1_sub = n.subscribe("/smartbuildings/temperature",1000,&SubscriberManager::temperature1_callback,this);
  ros::Subscriber humidity1_sub = n.subscribe("/smartbuildings/humidity",1000,&SubscriberManager::humidity1_callback,this);
  
  //subscribers to temperature sensor 2 topics
  ros::Subscriber temperature2_sub = n.subscribe("/smartbuildings/temperature2",1000,&SubscriberManager::temperature2_callback,this);
  ros::Subscriber humidity2_sub = n.subscribe("/smartbuildings/humidity2",1000,&SubscriberManager::humidity2_callback,this);
  
  //subscriber to door passage detector
  ros::Subscriber door_sub = n.subscribe("/smartbuildings/door",1000,&SubscriberManager::door_callback,this);
  ros::spin(); 
}


