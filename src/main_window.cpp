/**
 * @author Christos Karaiskos
 *
 * @brief Implementation for the qt gui.
 *
 * @date July 2014
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "../include/gui_smart_buildings_project/main_window.hpp"
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_legend.h"
#include <QVector>
#include <iostream>
#include <std_msgs/String.h>
/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace gui_smart_buildings_project {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/
MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
  ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
  //QObject::connect(ui.actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt())); // qApp is a global variable for the application
  peopleCounter = 0;
  previous = "";
  //plot->setAutoReplot(true);
  ui.qwtPlot->setTitle("Sensor Graph");
  ui.qwtPlot->setAxisTitle(QwtPlot::xBottom,"Time");
  QwtLegend *legend = new QwtLegend;
  legend->setFrameStyle(QFrame::Box|QFrame::Sunken);
  ui.qwtPlot->insertLegend(legend, QwtPlot::BottomLegend);
  //create plots for each sensor
  tempr1_curve = new QwtPlotCurve("Temperature 1 Curve");
  tempr2_curve = new QwtPlotCurve("Temperature 2 Curve");
  humid1_curve = new QwtPlotCurve("Humidity 1 Curve");
  humid2_curve = new QwtPlotCurve("Humidity 2 Curve");
  lux1_curve = new QwtPlotCurve("Luminosity 1 Curve");
  lux2_curve = new QwtPlotCurve("Luminosity 2 Curve");
  ir1_curve = new QwtPlotCurve("IR 1 Curve");
  ir2_curve = new QwtPlotCurve("IR 2 Curve");
  broadb1_curve = new QwtPlotCurve("Broadband 1 Curve");
  broadb2_curve = new QwtPlotCurve("Broadband 2 Curve");

  //set curve colors
  tempr1_curve->setPen(* new QPen(Qt::blue));
  tempr2_curve->setPen(* new QPen(Qt::green));
  humid1_curve->setPen(* new QPen(Qt::red));
  humid2_curve->setPen(* new QPen(Qt::magenta));
  lux1_curve->setPen(* new QPen(Qt::black));
  lux2_curve->setPen(* new QPen(Qt::darkYellow));
  ir1_curve->setPen(* new QPen(Qt::cyan));
  ir2_curve->setPen(* new QPen(Qt::darkCyan));
  broadb1_curve->setPen(* new QPen(Qt::darkMagenta));
  broadb2_curve->setPen(* new QPen(Qt::darkBlue));

  tempr1_curve->attach(ui.qwtPlot);
  tempr2_curve->attach(ui.qwtPlot);
  humid1_curve->attach(ui.qwtPlot);
  humid2_curve->attach(ui.qwtPlot);
  lux1_curve->attach(ui.qwtPlot);
  lux2_curve->attach(ui.qwtPlot);
  ir1_curve->attach(ui.qwtPlot);
  ir2_curve->attach(ui.qwtPlot);
  //broadb1_curve->attach(ui.qwtPlot);
  //broadb2_curve->attach(ui.qwtPlot);

  s = new SubscriberManager();
  boost::thread workerThread(&SubscriberManager::subscribeToAllTopics,s,argc,argv);
    
  //setWindowIcon(QIcon(":/images/icon.png"));
  ui.tab_manager->setCurrentIndex(0);
  QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

  boost::this_thread::sleep(boost::posix_time::seconds(5));

  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()),SLOT(updateSensorInfo()));
    timer->start(1000);


}

MainWindow::~MainWindow() {}

/*****************************************************************************
** Implementation [Slots]
*****************************************************************************/

void MainWindow::showNoMasterMessage() {
	QMessageBox msgBox;
	msgBox.setText("Couldn't find the ros master.");
	msgBox.exec();
    close();
}

void MainWindow::updateSensorInfo() {

    double t1 = s->getTemperatureFromSensor1();
    double t2 = s->getTemperatureFromSensor2();
    double h1 = s->getHumidityFromSensor1();
    double h2 = s->getHumidityFromSensor2();
    double l1 = s->getLuxFromSensor1();
    double l2 = s->getLuxFromSensor2();
    double b1 = s->getBroadbandFromSensor1();
    double b2 = s->getBroadbandFromSensor2();
    double i1 = s->getIRFromSensor1();
    double i2 = s->getIRFromSensor2();
    std_msgs::String d = s->getDoorSensor();

  if (t1 > 0 && t1 < 50) {
      ui.tempr1->setText(QString::number(t1));
      tempr1.append(s->getTemperatureFromSensor1());
  }
  if (t2 > 0 && t2 < 50) {
      ui.tempr2->setText(QString::number(t2));
      tempr2.append(s->getTemperatureFromSensor2());
  }
  if (h1 > 0 && h1 < 70)
  {
      ui.humid1->setText(QString::number(h1));
      humid1.append(s->getHumidityFromSensor1());
  }
  if (h2 > 0 && h2 < 70) {
      ui.humid2->setText(QString::number(h2));
      humid2.append(s->getHumidityFromSensor2());
  }
  if (l1 > 0) {
      ui.lux1->setText(QString::number(l1));
      lux1.append(s->getLuxFromSensor1());
  }
  if (l2 > 0) {
      ui.lux2->setText(QString::number(l2));
      lux2.append(s->getLuxFromSensor2());
  }
  if (b1 > 0) {
      ui.broadb1->setText(QString::number(b1));
      broadb1.append(s->getBroadbandFromSensor1());
  }
  if (b2 > 0) {
      ui.broadb2->setText(QString::number(b2));
      broadb2.append(s->getBroadbandFromSensor2());
  }
  if (i1 > 0) {
      ui.ir1->setText(QString::number(i1));
      ir1.append(s->getIRFromSensor1());
  }
  if (i2 > 0) {
      ui.ir2->setText(QString::number(i2));
      ir2.append(s->getIRFromSensor2());
  }
  ui.door->setText(QString::number(peopleCounter));
  std::cout << s->getDoorSensor().data << std::endl;
  if (previous.compare(s->getDoorSensor().data) && s->getDoorSensor().data[0] == 'I') {
      previous = s->getDoorSensor().data;

      peopleCounter++;
      door.append(peopleCounter);
  }
  else if (previous.compare(s->getDoorSensor().data) && s->getDoorSensor().data[0] == 'O') {
      previous = s->getDoorSensor().data;
      peopleCounter--;
      if ( peopleCounter < 0) peopleCounter = 0;
      door.append(peopleCounter);
  }

  if (tempr1.size() == 300) {
      if (t1 > 0)tempr1.remove(0);
      if (t2 > 0)tempr2.remove(0);
      if (h1 > 0)humid1.remove(0);
      if (h2 > 0)humid2.remove(0);
      if (l1 > 0)lux1.remove(0);
      if (l2 > 0)lux2.remove(0);
      if (b1 > 0) broadb1.remove(0);
      if (b2 > 0) broadb2.remove(0);
      if (i1 > 0) ir1.remove(0);
      if (i2 > 0) ir2.remove(0);
      //door.remove(0);
  }
  //append new values
  ui.qwtPlot->setAxisAutoScale(0);

  QVector<double> x;
  for (int i=0; i < tempr1.size(); i++)
      x.append(i);

  tempr1_curve->setData(x,tempr1);
  tempr2_curve->setData(x,tempr2);
  humid1_curve->setData(x,humid1);
  humid2_curve->setData(x,humid2);
  lux1_curve->setData(x,lux1);
  lux2_curve->setData(x,lux2);
  ir1_curve->setData(x,ir1);
  ir2_curve->setData(x,ir2);
  //broadb1_curve->setData(x,broadb1);
  //broadb2_curve->setData(x,broadb2);
  ui.qwtPlot->replot();

}

/*****************************************************************************
** Implementation [Menu]
*****************************************************************************/

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::about(this, tr("About ..."),tr("<h2>PACKAGE_NAME Test Program 0.10</h2>"));
}

/*****************************************************************************
** Implementation [Configuration]
*****************************************************************************/

void MainWindow::closeEvent(QCloseEvent *event)
{
	delete s;
	QMainWindow::closeEvent(event);
}

}  // namespace gui_smart_buildings_project

