/*
 * Logger for messages published by a set of ROS nodes. A running ROS master is required
 * on the same network.  The resulting bag can be played back through the 'rosbag play' command.
 * @date 23-07-2014
 * @author Christos Karaiskos
*/
#include <iostream>
#include <string>

#include "bag_writer.h"

#include "ros/ros.h"
#include "ros/master.h"

#include "rosbag/bag.h"
#include "rosbag/recorder.h"
#include "rosbag/exceptions.h"

using namespace std;
using namespace sml;

const unsigned int MAX_FILENAME_LENGTH = 15;
const unsigned int MAX_BAG_SIZE_MB = 1024;
const unsigned int MAX_DURATION_SECS = 24*60*60;

SensorMsgLogger::SensorMsgLogger(int maxDuration, int maxSizeMB, bool recordAll, string bagfilename)
{
  setMaxLoggingDurationInSeconds(maxDuration);
  setMaxBagSizeInMB(maxSizeMB);
  setRecordAllTopics(recordAll);
  setBagFileName(bagfilename);
}

void SensorMsgLogger::listAllTopics() {
  ros::master::V_TopicInfo topics;
  ros::master::getTopics(topics);
  cout << "**************** TOPICS ******************" << endl;
  for (ros::master::V_TopicInfo::iterator it = topics.begin(); it != topics.end(); it++) {
    cout << it->name << endl;
  }
  cout << "******************************************" << endl;
}

vector<string> SensorMsgLogger::getAllTopics() {
  ros::master::V_TopicInfo topics;
  ros::master::getTopics(topics);
  vector<string> vect;
  for (ros::master::V_TopicInfo::iterator it = topics.begin(); it != topics.end(); it++) {
    vect.push_back(it->name);
  }
  return vect;
}

void SensorMsgLogger::logAllTopics() {
  rosbag::Recorder* recorder = new rosbag::Recorder(opts);
  //cout << "Started Logging All Topics..." <<endl;
  recorder->run();
  delete recorder;
}

bool SensorMsgLogger::isTopicAvailable(const string & s) {
  ros::master::V_TopicInfo topics;
  ros::master::getTopics(topics);
  for (ros::master::V_TopicInfo::iterator it = topics.begin(); it != topics.end(); it++) {
    if (s == it->name) return true;
  }
  return false;
}

void SensorMsgLogger::logTopicsWithNames(vector<string> & topics) {
  for (vector<string>::iterator it = topics.begin();it!=topics.end();it++) {
    if (isTopicAvailable(*it)) {
	cout << "Started Logging Topic with Name " << *it << endl;
	opts.topics.push_back(*it);
      }
    else cout << "Topic with name " << *it << " is not available." << endl;
  }
  rosbag::Recorder* recorder = new rosbag::Recorder(opts);
  recorder->run();
     
  delete recorder;
}

//getters
ros::Duration SensorMsgLogger::getMaxLoggingDurationInSeconds() {
  return opts.max_duration;
}

int SensorMsgLogger::getMaxBagSize() {
  return opts.max_size;
}

bool SensorMsgLogger::doesRecordAllTopics() {
  return opts.record_all;
}

string SensorMsgLogger::getBagFileName() {
  return opts.prefix;
}   

//set logger to stop after recording a maximum of 'duration' secs
void SensorMsgLogger::setMaxLoggingDurationInSeconds(int duration) {
  if (duration < 1 || duration > MAX_DURATION_SECS) {
    cout << "Please provide a maximum record duration between 1 and " << MAX_DURATION_SECS <<" secs";
    return;
  }
  opts.max_duration = ros::Duration(duration);
}

//set logger to stop after recording a maximum of sizeMB megabytes
void SensorMsgLogger::setMaxBagSizeInMB(int sizeMB) {
  if (sizeMB < 1 || sizeMB > MAX_BAG_SIZE_MB) {
    cout << "Please provide a maximum bag size between 1 and " << MAX_BAG_SIZE_MB <<" MB";
    return;
  }
  opts.max_size = sizeMB * 1024 * 1024;
}

//set logger to record all msgs on all topics
void SensorMsgLogger::setRecordAllTopics(bool recordAll) {
  opts.record_all = recordAll;
}

//set the logging filename
void SensorMsgLogger::setBagFileName(string filename) {
  if (filename.length() > MAX_FILENAME_LENGTH) {
    cout << "Please provide a shorter file name (max = " << MAX_FILENAME_LENGTH <<  " %d characters)";
    return;
  }
  opts.prefix = filename;
  opts.append_date = false;
}
